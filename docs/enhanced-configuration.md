<!--
SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-License-Identifier: Apache-2.0
-->

<h1>Enhanced configuration use cases for openDesk</h1>

# Overview

The following enhanced configuration use cases are described in separate documents.

- [Separate mail & Matrix domain](./enhanced-configuration/separate-mail-matrix-domain.md)
- [Federation with external identity provider](./enhanced-configuration/idp-federation.md)
- [Matrix federation](./enhanced-configuration/matrix-federation.md)
- [Groupware migration from M365 to openDesk](./enhanced-configuration/groupware-migration.md)
- [Self-signed certificate and custom Certificate Authority (CA)](./enhanced-configuration/self-signed-certificates.md)
- [GitOps deployments using Argo CD](./enhanced-configuration/gitops.md)
