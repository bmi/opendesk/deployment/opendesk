{{/*
SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
*/}}
---
architecture: "standalone"

auth:
  password: {{ .Values.secrets.redis.password | quote }}

global:
  imagePullSecrets:
    {{ .Values.global.imagePullSecrets | toYaml | nindent 4 }}
  storageClass: {{ coalesce .Values.persistence.storages.redis.storageClassName .Values.persistence.storageClassNames.RWO | quote }}

image:
  registry: {{ coalesce .Values.repositories.image.dockerHub .Values.global.imageRegistry .Values.images.redis.registry | quote }}
  repository: {{ .Values.images.redis.repository | quote }}
  tag: {{ .Values.images.redis.tag | quote }}
  pullPolicy: {{ .Values.global.imagePullPolicy | quote }}

master:
  containerSecurityContext:
    privileged: false
    readOnlyRootFilesystem: true
    runAsUser: 1001
    runAsGroup: 1001
    runAsNonRoot: true
    allowPrivilegeEscalation: false
    seccompProfile:
      type: "RuntimeDefault"
    capabilities:
      drop:
        - "ALL"
    seLinuxOptions:
      {{ .Values.seLinuxOptions.redis | toYaml | nindent 6 }}
  count: {{ .Values.replicas.redis }}
  persistence:
    size: {{ .Values.persistence.storages.redis.size | quote }}
  podAnnotations: {}
  resources:
    {{ .Values.resources.redis | toYaml | nindent 4 }}

metrics:
  enabled: false

sentinel:
  enabled: false

...
